public enum CvmCondition {
    Always(0),
    OnlyApplyUnattended(1),
    NotUnattendedNotManualCashNotCashback(2),
    AlwaysTryThis(3),
    IfManual(4),
    IfCashback(5),
    IfLess(6),
    IfMore(7),
    // ...
    ;

    byte type;

    CvmCondition(int type) {
        this.type = (byte) type;
    }

    byte getType() {
        return type;
    }
}
