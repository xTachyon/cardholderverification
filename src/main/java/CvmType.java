public enum CvmType {
    Fail(0),
    OfflinePlaintextPin(1),
    OnlinePin(2),
    OfflinePlaintextPinAndPaper(3),
    OfflineEncipheredPin(4),
    OfflineEncipheredPinAndSignature(5),
    PaperSignature(30),
    Approve(31);

    int type;

    CvmType(int type) {
        this.type = type;
    }

    int getType() {
        return type;
    }
}
