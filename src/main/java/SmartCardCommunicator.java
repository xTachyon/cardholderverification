import com.sun.javacard.apduio.Apdu;
import com.sun.javacard.apduio.CadT1Client;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class SmartCardCommunicator {
    public static final byte WALLET_CLA = (byte) 0x80;
    private final Apdu apdu;
    private final boolean trueMode;
    private CadT1Client cad;
    private boolean pinEnteredOk;

    public SmartCardCommunicator(boolean trueMode) throws Exception {
        this.trueMode = trueMode;
        apdu = new Apdu();

        if (trueMode) {
            Socket socket = new Socket("localhost", 9025);
            socket.setTcpNoDelay(true);

            BufferedInputStream input = new BufferedInputStream(socket.getInputStream());
            BufferedOutputStream output = new BufferedOutputStream(socket.getOutputStream());

            cad = new CadT1Client(input, output);
            cad.powerUp();

            executeCommandsFromFile("cap-Waller.script");
        }
    }

    int numberDigits(int n) {
        if (n == 0) {
            return 1;
        }
        int digits = 0;
        while (n != 0) {
            n /= 10;
            digits++;
        }
        return digits;
    }


    public void enterPin(int pin) throws Exception {
        byte[] buffer = new byte[numberDigits(pin)];

        for (int i = 0; pin != 0; ++i) {
            buffer[i] = (byte) (pin % 10);
            pin /= 10;
        }

        for (int i = 0; i < buffer.length / 2; ++i) {
            int index = buffer.length - i - 1;

            byte temp = buffer[index];
            buffer[index] = buffer[i];
            buffer[i] = temp;
        }

        send(Operation.VerifyPin, buffer);

        pinEnteredOk = true;

        // the response is "returned" by exception
    }

    private List<CvmType> getSelfCvm() {
        return Arrays.asList(CvmType.OfflinePlaintextPin, CvmType.Approve);
    }

    public List<CvmInfo> getCvm() throws Exception {
        send(Operation.Cvm, new byte[0]);

        if (!trueMode) {
            CvmCode code = new CvmCode((byte)(CvmType.Approve.getType() | 0b01000000), CvmCondition.IfLess.getType());
            CvmInfo first = new CvmInfo(50, 0, code);

            code = new CvmCode((byte)(CvmType.OfflinePlaintextPin.getType() | 0b01000000), CvmCondition.IfMore.getType());
            CvmInfo second = new CvmInfo(0, 0, code);

            return Arrays.asList(first, second);
        }

        byte[] data = apdu.getDataOut();
        List<CvmInfo> result = new ArrayList<>();
        if (data == null) {
            return result;
        }
        DataInputStream stream = new DataInputStream(new ByteArrayInputStream(data));

        while (stream.available() > 0) {
            int x = stream.readInt();
            int y = stream.readInt();
            byte type = stream.readByte();
            byte condition = stream.readByte();
            result.add(new CvmInfo(x, y, new CvmCode(type, condition)));
        }
        return result;
    }

    private CvmType chooseCvm(List<CvmInfo> smartcard, int value) throws Exception {
        List<CvmType> self = getSelfCvm();

        for (CvmInfo info : smartcard) {
            CvmCode code = info.getCode();
            CvmType type = code.getType();
            CvmCondition condition = code.getCondition();
            if (self.contains(type)) {
                if (type == CvmType.Fail) {
                    throw new Exception("cvm fail will always fail");
                }
                if (type == CvmType.Approve) {
                    if (condition == CvmCondition.IfLess && info.getTransactionX() > value) {
                        return type;
                    }
                    if (condition == CvmCondition.IfMore && info.getTransactionX() < value) {
                        return type;
                    }
                }
                if (type == CvmType.OfflinePlaintextPin) {
                    if (condition == CvmCondition.IfLess && info.getTransactionX() > value) {
                        return type;
                    }
                    if (condition == CvmCondition.IfMore && info.getTransactionX() < value) {
                        return type;
                    }
                }
            }
            if (code.failIfUnsuccessful()) {
                throw new Exception("fail bit is set");
            }
        }
        throw new Exception("no cvm matched");
    }

    public void debit(int value) throws Exception {
        if (value > Byte.MAX_VALUE) {
            throw new Exception("value to big to fit in a byte");
        }
        List<CvmInfo> cvm = getCvm();
        CvmType chosenCvm = chooseCvm(cvm, value);
        if (chosenCvm == CvmType.OfflinePlaintextPin && !pinEnteredOk) {
            throw new Exception("you need to enter pin for this transaction");
        }

        byte[] buffer = new byte[1];
        buffer[0] = (byte) value;

        send(Operation.Debit, buffer);
    }

    public void addCredit(int value) throws Exception {
        if (value > Byte.MAX_VALUE) {
            throw new Exception("value to big to fit in a byte");
        }
        byte[] buffer = new byte[1];
        buffer[0] = (byte) value;

        send(Operation.Credit, buffer);
    }

    private void send(Operation operation, byte[] buffer) throws Exception {
        apdu.command[Apdu.CLA] = WALLET_CLA;
        apdu.command[Apdu.INS] = operation.getValue();
        apdu.command[Apdu.P1] = 0;
        apdu.command[Apdu.P2] = 0;
        apdu.command[Apdu.P3] = 0;

        byte[] newBuffer = new byte[buffer.length + 2];
        newBuffer[0] = (byte) buffer.length;
        newBuffer[newBuffer.length - 1] = 0x7F;
        System.arraycopy(buffer, 0, newBuffer, 1, buffer.length);

        apdu.setDataIn(newBuffer);
        printBuffer(newBuffer);

        if (trueMode) {
            cad.exchangeApdu(apdu);

            if (apdu.getStatus() != 0x9000) {
                throw new Exception("card returned error " + apdu.getStatus());
            }
        }
    }

    private void printBuffer(byte[] buffer) {
        StringBuilder builder = new StringBuilder("buffer = ");
        for (int i = 0; i < 4; ++i) {
            String string = String.format("0x%02X ", apdu.command[i]);
            builder.append(string);
        }
        for (byte b : buffer) {
            String string = String.format("0x%02X ", b);
            builder.append(string);
        }
        builder.append(";");
        System.out.println(builder);
    }

    private void executeCommandsFromFile(String file) throws Exception {
        List<String> lines = Files
                .lines(Paths.get(file))
                .map(String::trim)
                .filter(x -> !x.startsWith("//"))
                .filter(x -> x.endsWith(";"))
                .map(x -> x.substring(0, x.length() - 1))
                .collect(Collectors.toList());

        for (String line : lines) {
            List<Byte> bytes = Arrays.stream(line.split(" "))
                    .map(x -> x.substring(2))
                    .map(x -> Integer.parseInt(x, 16))
                    .map(x -> (byte) x.intValue())
                    .collect(Collectors.toList());

            byte[] dataBytes = new byte[bytes.size() - 4];
            for (int i = 0; i < dataBytes.length; ++i) {
                dataBytes[i] = bytes.get(i + 4);
            }

            apdu.command[Apdu.CLA] = bytes.get(0);
            apdu.command[Apdu.INS] = bytes.get(1);
            apdu.command[Apdu.P1] = bytes.get(2);
            apdu.command[Apdu.P2] = bytes.get(3);
            apdu.setDataIn(dataBytes);

            cad.exchangeApdu(apdu);

            if (apdu.getStatus() != 0x9000) {
                throw new Exception("card returned error " + apdu.getStatus() + " for line " + line);
            }
        }
    }
}
