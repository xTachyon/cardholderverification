public enum Operation {
    VerifyPin(0x20),
    Credit(0x30),
    Debit(0x40),
    Cvm(0x8E);

    private final int value;

    Operation(int value) {
        this.value = value;
    }

    byte getValue() {
        return (byte) value;
    }
}
