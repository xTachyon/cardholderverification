public class CvmCode {
    private final byte type;
    private final byte condition;

    public CvmCode(byte type, byte condition) {
        this.type = type;
        this.condition = condition;
    }

    boolean failIfUnsuccessful() {
        return ((type >> 6) & 1) == 0;
    }

    boolean moveNextIfUnsuccessful() {
        return !failIfUnsuccessful();
    }

    CvmType getType() {
        byte type = (byte) (this.type & 0b111111);
        if (type <= 5) {
            return CvmType.values()[type];
        }
        if (type == 30) {
            return CvmType.PaperSignature;
        }
        return CvmType.Approve;
    }

    CvmCondition getCondition() {
        return CvmCondition.values()[condition];
    }
}

