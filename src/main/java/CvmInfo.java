public class CvmInfo {
    private final int transactionX;
    private final int transactionY;
    private final CvmCode code;

    public CvmInfo(int transactionX, int transactionY, CvmCode code) {
        this.transactionX = transactionX;
        this.transactionY = transactionY;
        this.code = code;
    }

    public int getTransactionX() {
        return transactionX;
    }

    public int getTransactionY() {
        return transactionY;
    }

    public CvmCode getCode() {
        return code;
    }
}
