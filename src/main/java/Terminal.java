import java.util.List;
import java.util.Scanner;

public class Terminal {
    private final SmartCardCommunicator communicator;
    private final Scanner scanner;
    private boolean hasPin;

    public Terminal() throws Exception {
        communicator = new SmartCardCommunicator(false);
        scanner = new Scanner(System.in);
    }

    private void doThings() throws Exception {
        String line = scanner.nextLine();
        String[] tokens = line.split(" ");

        String command = tokens[0];
        if (command.equals("credit")) {
            int value = Integer.parseInt(tokens[1]);
            communicator.addCredit(value);
        } else if (command.equals("debit")) {
            int value = Integer.parseInt(tokens[1]);

            communicator.debit(value);
        } else if (command.equals("pin")) {
            int value = Integer.parseInt(tokens[1]);

            communicator.enterPin(value);
            hasPin = true;
        } else {
            System.out.println("no command with that name");
        }
    }

    public void run() {
        while (true) {
            try {
                doThings();
            } catch (Exception e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        }
    }

}
